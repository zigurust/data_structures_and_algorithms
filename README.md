# Data structures and algorithms

This repository contains some implementations of data structures and algorithms in Zig and Rust.

## License

All implementations are licensed under [MIT](./LICENSE) if no other licensed is mentioned.

