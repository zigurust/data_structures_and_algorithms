const std = @import("std");

const solve = @import("lines.zig").solve;

fn sample() void {
    var lines = [_]u7{ 3, 2, 5 };
    const new_people: u7 = 4;
    solve(&lines, new_people);
    for (lines) |line| {
        std.debug.print("{}\n", .{line});
    }
}

pub fn main() !void {
    const stdout = std.io.getStdOut();
    try stdout.writeAll("Add number of lines and number of new people. Use space as delimiter.\n");
    const stdin = std.io.getStdIn();
    var buffer: [100]u8 = undefined;
    const optional_line = try stdin.reader().readUntilDelimiterOrEof(&buffer, '\n');
    if (optional_line == null) {
        return error.InputLineNull;
    }
    const line = optional_line.?;
    if (line.len == 0) {
        return error.NoInput;
    }
    var index_with_space: usize = 0;
    while (index_with_space < line.len) : (index_with_space += 1) {
        if (line[index_with_space] == 0x20) {
            break;
        }
    }
    if (index_with_space == line.len) {
        return error.NoSpaceDelimiter;
    }
    const number_of_lines = std.fmt.parseInt(u7, line[0..index_with_space], 10) catch {
        return error.IntegerParseError;
    };
    const number_of_people = std.fmt.parseInt(u7, line[index_with_space + 1 ..], 10) catch {
        return error.IntegerParseError;
    };
    try stdout.writer().print("Number of lines {}\n", .{number_of_lines});
    try stdout.writer().print("Number of people {}\n", .{number_of_people});

    try stdout.writer().print("Add the number of people for each line. Use space as delimiter\n", .{});
    const optional_line_2 = try stdin.reader().readUntilDelimiterOrEof(&buffer, '\n');
    if (optional_line_2 == null) {
        return error.InputLineNull;
    }
    const line_2 = optional_line_2.?;
    if (line.len == 0) {
        return error.NoInput;
    }

    var lines = [_]u7{0} ** 127;
    var lines_index: usize = 0;
    var i: usize = 0;
    var start: usize = 0;
    index_with_space = 0;
    while (i < line_2.len) : (i += 1) {
        if (line_2[i] == 0x20) {
            index_with_space = i;
            const number_of_people_in_line = try std.fmt.parseInt(u7, line_2[start..index_with_space], 10);
            start = index_with_space + 1;
            if (lines_index < lines.len) {
                lines[lines_index] = number_of_people_in_line;
                lines_index += 1;
            } else {
                break;
            }
        }
    }
    // parse last integer
    if (lines_index < lines.len) {
        const number_of_people_in_line = try std.fmt.parseInt(u7, line_2[start..], 10);
        lines[lines_index] = number_of_people_in_line;
    }

    solve(lines[0..number_of_lines], number_of_people);
    for (lines[0..number_of_lines]) |l| {
        std.debug.print("{}\n", .{l});
    }
}

test "tests" {
    @import("std").testing.refAllDecls(@This());
}
