pub fn solve(lines: []u7, new_people: u7) void {
    var i: u7 = 0;
    while (i < new_people) : (i += 1) {
        const shortest_index = shortestLineIndex(lines);
        lines[shortest_index] += 1;
    }
}

test "solve" {
    var lines = [_]u7{ 3, 2, 5 };
    const new_people: u7 = 4;
    solve(&lines, new_people);
    const expectEqual = @import("std").testing.expectEqual;
    try expectEqual(@as(u7, 5), lines[0]);
    try expectEqual(@as(u7, 4), lines[1]);
    try expectEqual(@as(u7, 5), lines[2]);
}

fn shortestLineIndex(lines: []const u7) usize {
    var shortest_line_index: usize = 0;
    var shortest_value = lines[shortest_line_index];
    for (lines) |value, i| {
        if (value < shortest_value) {
            shortest_value = value;
            shortest_line_index = i;
        }
    }
    return shortest_line_index;
}

test "shortestLineIndex" {
    const lines = [4]u7{ 3, 4, 2, 1 };
    var index: usize = shortestLineIndex(&lines);
    const expectEqual = @import("std").testing.expectEqual;
    try expectEqual(@as(usize, 3), index);
}
