use lines::solve;
use std::error::Error;
use std::io;

fn sample() {
    let mut lines: [u8; 3] = [3, 2, 5];
    let new_people = 4;
    solve(&mut lines, new_people);
    println!("sample = {lines:?}");
}

fn main() -> Result<(), Box<dyn Error>> {
    sample();
    println!("Enter number or lines and number of people divided with space.");

    let mut buffer = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut buffer)?;
    let inputs = buffer
        .trim_end()
        .split(" ")
        .map(|x| u8::from_str_radix(x, 10).unwrap_or(0))
        .collect::<Vec<u8>>();

    let number_of_lines = inputs[0];
    let new_people = inputs[1];

    let mut lines = [0; 127];

    buffer.clear();
    println!("Set the number of people per line, for all of the {number_of_lines}, each number divided by a space.");
    stdin.read_line(&mut buffer)?;
    let input_number_of_people_per_line = buffer
        .trim_end()
        .split(" ")
        .map(|x| u8::from_str_radix(x, 10).unwrap_or(0))
        .collect::<Vec<u8>>();

    let number_of_lines: usize = number_of_lines.into();

    for i in 0..number_of_lines {
        lines[i] = input_number_of_people_per_line[i];
    }

    println!("Number or new people={new_people}");
    print!("lines=");
    for i in 0..number_of_lines {
        let line = lines[i];
        print!("{line},");
    }
    println!("");

    solve(
        &mut lines[0..Into::<usize>::into(number_of_lines)],
        new_people,
    );

    print!("lines after solve=");
    for i in 0..number_of_lines {
        let line = lines[i];
        print!("{line},");
    }
    println!("");

    Ok(())
}
