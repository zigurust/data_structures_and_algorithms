pub fn solve(lines: &mut [u8], new_people: u8) {
    for _i in 0..new_people {
        let shortest_index = shortest_line_index(lines);
        lines[shortest_index] += 1;
    }
}

fn shortest_line_index(lines: &[u8]) -> usize {
    let mut shortest_line_index: usize = 0;
    let mut shortest_value = lines[shortest_line_index];
    for (i, line) in lines.iter().enumerate() {
        if line < &shortest_value {
            shortest_value = *line;
            shortest_line_index = i;
        }
    }
    shortest_line_index
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_shortest_line_index() {
        let lines = [3, 4, 2, 1];
        let index = shortest_line_index(&lines);
        assert_eq!(3, index);
    }

    #[test]
    fn test_solve() {
        let mut lines: [u8; 3] = [3, 2, 5];
        let new_people = 4;
        solve(&mut lines, new_people);
        assert_eq!(5, lines[0]);
        assert_eq!(4, lines[1]);
        assert_eq!(5, lines[2]);
    }
}
