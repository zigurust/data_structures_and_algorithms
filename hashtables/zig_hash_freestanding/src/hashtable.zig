pub const HashTableErrors = error{
    ElementBufferTooSmall,
    CellarReferenceBufferTooSmall,
    CapacityExceeded,
};

pub fn ConstElementIndexPair(comptime T: type) type {
    return struct {
        element: *const T,
        index: usize,
    };
}

test "BucketElementIndexPair" {
    // Arrange
    const expectEqual = @import("std").testing.expectEqual;
    const ConstU8IndexPair = ConstElementIndexPair(u8);
    const element: u8 = 5;
    // Act
    const pair = ConstU8IndexPair{
        .element = &element,
        .index = 15,
    };
    // Assert
    try expectEqual(element, pair.element.*);
}

pub fn BucketIterator(comptime T: type) type {
    return struct {
        const Self = @This();
        elements: []const ?T,
        bucket_indices: []const ?usize,
        current_index: ?usize,
        pub fn next(self: *Self) ?ConstElementIndexPair(T) {
            var index: usize = 0;
            if (self.current_index) |current_index| {
                index = current_index;
                @import("std").debug.print("index = {}. current_index = {?}\n", .{ index, self.current_index });
            } else {
                return null;
            }

            if (index >= self.elements.len) {
                self.current_index = null;
                return null;
            }

            const optional_element = self.elements[index];
            if (optional_element) |_| {
                const address = @ptrToInt(&self.elements[index]);
                const ptr = @intToPtr(*const T, address);
                const PairType = ConstElementIndexPair(T);
                const pair = PairType{
                    .index = index,
                    .element = ptr,
                };
                self.current_index = self.bucket_indices[index];
                return pair;
            }

            return null;
        }
    };
}

test "BucketIterator.next" {
    // Arrange
    const testing = @import("std").testing;
    const expectEqual = testing.expectEqual;
    const expect = testing.expect;
    const elements: [3]?u8 = [_]?u8{ 3, 4, 6 };
    const bucket_indices: [3]?usize = [_]?usize{ null, 2, null };
    var index: usize = 0;
    const BucketIteratorU8 = BucketIterator(u8);
    var iterator = BucketIteratorU8{
        .elements = elements[0..],
        .bucket_indices = bucket_indices[0..],
        .current_index = index,
    };
    // Act
    const optional_element_1 = iterator.next();
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    const optional_element_2 = iterator.next();
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    iterator.current_index = 1;
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    const optional_element_3 = iterator.next();
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    const optional_element_4 = iterator.next();
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    const optional_element_5 = iterator.next();
    @import("std").debug.print("{?}\n", .{optional_element_1});
    if (optional_element_1) |e| {
        @import("std").debug.print("{}\n", .{e.element.*});
    }
    // Assert
    if (optional_element_1) |element| {
        @import("std").debug.print("{}\n", .{element.element.*});
        try expectEqual(elements[0], element.element.*);
        try expectEqual(element.index, 0);
    } else {
        try expect(false);
    }
    if (optional_element_2) |_| {
        try expect(false);
    }
    if (optional_element_3) |e| {
        try expectEqual(elements[1], e.element.*);
        try expectEqual(e.index, 1);
    } else {
        try expect(false);
    }
    if (optional_element_4) |e| {
        try expectEqual(elements[2], e.element.*);
        try expectEqual(e.index, 2);
    } else {
        try expect(false);
    }
    if (optional_element_5) |_| {
        try expect(false);
    }
}

pub fn HashTable(comptime T: type, comptime max_number_of_elements: usize, comptime cellar_size: usize) type {
    return struct {
        const Self = @This();
        const cellar_length = cellar_size;
        const max_num_elem = max_number_of_elements;
        const max_length = max_number_of_elements + cellar_size;
        elements: *[max_length]?T,
        bucket_indices: *[max_length]?usize,
        len: usize = 0,
        hasher: *const fn (t: *const T) usize,

        pub fn insert(self: *Self, t: T) HashTableErrors!void {
            if (self.len > max_length) {
                return HashTableErrors.CapacityExceeded;
            }
            const hash_index = self.hashIndex(&t);
            var optional_element = &self.elements[hash_index];
            if (optional_element.*) |_| {
                const optional_bucket_index = self.bucket_indices[hash_index];
                if (optional_bucket_index) |bucket_index| {
                    const next_free_index = self.findNextFreeIndex(bucket_index);
                    self.elements[next_free_index] = t;
                    // TODO add Iterator to get last element of bucket
                } else {
                    // the cellar is located at the end of the elements array.
                    const next_free_index = self.findNextFreeIndex(max_num_elem);
                    self.bucket_indices[hash_index] = next_free_index;
                    self.elements[next_free_index] = t;
                }
            } else {
                optional_element.* = t;
            }
            self.len += 1;
        }

        fn hashIndex(self: *Self, t: *const T) usize {
            const hash_index = self.hasher(t) % max_num_elem;
            return hash_index;
        }

        fn findNextFreeIndex(self: *Self, index: usize) usize {
            _ = index;
            _ = self;
            // TODO
            return 0;
        }
    };
}

test "Insert into empty hashtable" {
    // Arrange
    const HashTableU8 = HashTable(u8, 10, 2);
    var elements = [_]?u8{null} ** 12;
    var bucket_indices = [_]?usize{null} ** 12;
    const HashFunctions = struct {
        fn hash(t: *const u8) usize {
            return @intCast(usize, t.*);
        }
    };
    var hash_table = HashTableU8{
        .elements = &elements,
        .bucket_indices = &bucket_indices,
        .hasher = HashFunctions.hash,
    };

    // Act
    try hash_table.insert(0);

    // Assert
    const expectEqual = @import("std").testing.expectEqual;
    try expectEqual(hash_table.elements[0], 0);
    try expectEqual(hash_table.elements[1], null);
    try expectEqual(hash_table.len, 1);
}
