const std = @import("std");

const hashtable = @import("hashtable.zig");

fn hash(element: *const u8) usize {
    return @intCast(usize, element.*);
}

pub fn main() !void {
    std.debug.print("Hash table freestanding.\n", .{});

    const HashTableU8 = hashtable.HashTable(u8, 100, 20);

    var elements = [_]?u8{null} ** 120;
    var bucket_indices = [_]?usize{null} ** 120;

    var hash_table = HashTableU8{
        .elements = &elements,
        .bucket_indices = &bucket_indices,
        .hasher = hash,
    };

    try hash_table.insert(5);
}

test "all" {
    @import("std").testing.refAllDecls(@This());
}
